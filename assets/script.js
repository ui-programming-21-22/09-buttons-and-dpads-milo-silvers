console.log("helooooo :)")

//////////////////////////////////// Clock ////////////////////////////////////////

let divtime = document.getElementById("divtime");
function time()
{
    let date = new Date();
    let dd = date.getDate();
    let mm = date.getMonth();
    let yy = date.getFullYear();
    let hh = date.getHours();
    let min = date.getMinutes();
    let ss = date.getSeconds();

    let period = "AM";


    if(hh == 0)
    {
        hh = 12;
    }

    if(hh > 12)
    {
        hh = hh - 12;
        period = "PM";
    }

    console.log("Today's date: "+dd+
        "."+(mm+1)+
        "."+yy+
        " Time: "+hh+
        ":"+min+
        ":"+ss+
        ":" +period)

        divtime.innerHTML = "Today's date: "+dd+
        "."+(mm+1)+
        "."+yy+
        " Time: "+hh+
        ":"+min+
        ":"+ss+ 
        ":" +period;
}

setInterval(time, 1000);

//////////////////////////////////// Game - vars ////////////////////////////////////////

const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");

let gamerInput = new GamerInput("None");

let xpos = 20; //these are the position of the player, change them to make him spawn elsewhere
let ypos = 20;
let direction = 0; //this changes depending which way he faces, starts down then goes like sprite sheet, 1 = right, 2=left, 3 =up
let healthWidth = 100;
let healthHeight = 20;
let max = 100;
let health = 100;
let speed = 6;
let score = 100;
const SCALE = 2;

let hollowImg = new Image();
hollowImg.src = "assets/hollow.png";

//////////////////////////////////// functions ////////////////////////////////////////



function GamerInput(input)
{
    this.action = input;
}

function gameLoop()
{
    update(); //update before drawing
    draw();
    healthScore();
    drawHealthbar();
    window.requestAnimationFrame(gameLoop);    
}

function draw()
{
    context.clearRect(0, 0, canvas.width, canvas.height); //this goes first, as it needs to clear before drawing.   
    current = new Date().getTime();
    if (current - initial >= 100) {
        currentFrame = (currentFrame + 1) % frameCount;
        initial = current;
    }
    if (gamerInput.action !== "None")
    {
        context.drawImage(hollowImg, (128 * currentFrame), (127 * direction), 121, 127, xpos, ypos, 100, 100); 
    }
    else
    {
        context.drawImage(hollowImg, 0, (127 * direction), 121, 127, xpos, ypos, 100, 100); 
    }
}



function input(event) 
{
    if (event.type === "keydown") 
    {
        switch (event.keyCode) 
        {
            case 37:
                gamerInput = new GamerInput("Left");
                break;
            case 65:
                gamerInput = new GamerInput("Left");
                break;
            case 38:
                gamerInput = new GamerInput("Up");
                break; 
            case 87:
                gamerInput = new GamerInput("Up");
                break;
            case 39:
                gamerInput = new GamerInput("Right");  
                break;
            case 68:
                gamerInput = new GamerInput("Right"); 
                break;
            case 40:
                gamerInput = new GamerInput("Down");
                break;
            case 83:
                gamerInput = new GamerInput("Down");
                break;
            case 32:
                console.log("boom");
                document.getElementById('boom').play();
                break;
            default:
                gamerInput = new GamerInput("None");
        }
    }
    else
    {
        gamerInput = new GamerInput("None");
    }
}


function playerMVMT()
{
    if (gamerInput.action == "Up")
    {
        direction = 3;        
        if (ypos < 0)
        {
            console.log("top touched");
            health -= 1;
            score -= 1;
        }
        else 
        { 
            ypos -= speed; 
        }
        currentDirection = 3;
    }
    if (gamerInput.action == "Down")
    {
        direction = 0;
        if (ypos + 100 > canvas.height)
        {
            console.log("bottom touched");
            health -= 1;
            score -= 1;
        }
        else
        {
            ypos += speed;
        }
        currentDirection = 0;
    }
    if (gamerInput.action == "Left")
    {
        direction = 2;
        if (xpos < 0)
        {
            console.log("left touched");
            health -= 1;
            score -= 1;
        }
        else
        {
            xpos -= speed;
        }
        currentDirection = 2;
    }
    if (gamerInput.action == "Right")
    {
        direction = 1;
        if (xpos + 100 > canvas.width)
        {
            console.log("right touched");
            health -= 1;
            score -= 1;
        }
        else
        {
            xpos += speed;
        }
        currentDirection = 1;
    }       
    console.log(score);
}

function healthScore()
{
    context.font = "16px Arial";
    context.fillStyle = "white";
    context.fillText(score, 105, 18);
}

//////////////////////////////////// animations ////////////////////////////////////////

const width = 121;
const height = 127;

let currentLoopIndex = 0;
let frameCount = 6; //amount of frames in sprite
let initial = new Date().getTime();
let current; 
let currentFrame = 0;


function update()
{
    playerMVMT();

}

///////////////////////////////// D Pad //////////////////////////////////////////////

function clickDpadYellow()
{
    gamerInput = new GamerInput("Up");
}
function clickDpadGreen()
{
    gamerInput = new GamerInput("Down");
}
function clickDpadBlue()
{
    gamerInput = new GamerInput("Left");
}
function clickDpadRed()
{
    gamerInput = new GamerInput("Right");
}
function noMoving()
{
    gamerInput = new GamerInput("None");
}

//////////////////////////////// Nipple //////////////////////////////////////////////

var dynamic = nipplejs.create({
    color: 'red',
});

dynamic.on('added', function (evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
       //console.log("direction up");
       gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function (evt, data) {
        //console.log("direction down");
        gamerInput = new GamerInput("Down");
     });
     nipple.on('dir:left', function (evt, data) {
        //console.log("direction left");
        gamerInput = new GamerInput("Left");
     });
     nipple.on('dir:right', function (evt, data) {
        //console.log("direction right");
        gamerInput = new GamerInput("Right");
     });
     nipple.on('end', function (evt, data) {
        //console.log("mvmt stopped");
        gamerInput = new GamerInput("None");
     });
});

  
////////////////////////////////// Health Bar ///////////////////////////////////////

function drawHealthbar() 
{
    context.fillStyle = "black";

    context.fillRect(0, 0, healthWidth, healthHeight);
  
    context.fillStyle = "#00FF00";
    var fillVal = Math.min(Math.max(health / max, 0), 1);
    context.fillRect(0, 0, fillVal * healthWidth, healthHeight);
}

window.requestAnimationFrame(gameLoop);
window.addEventListener('keydown',input);
window.addEventListener('keyup',input);